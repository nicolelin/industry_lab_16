package ictgradschool.industry.lab16.ex02.gui;

import ictgradschool.industry.lab16.ex02.model.Course;
import ictgradschool.industry.lab16.ex02.model.CourseListener;

public class StatisticsPanelAdapter implements CourseListener{

	/**********************************************************************
	 * YOUR CODE HERE
	 */

	StatisticsPanel statsPanel;

	public StatisticsPanelAdapter(StatisticsPanel statsPanel) {
		this.statsPanel = statsPanel;
	}

	@Override
	public void courseHasChanged(Course course) {
		statsPanel.repaint();
	}

}
