package ictgradschool.industry.lab16.ex02.model;


import javax.swing.table.AbstractTableModel;

public class CourseAdapter extends AbstractTableModel implements CourseListener {

    /**********************************************************************
     * YOUR CODE HERE
     */

    /* TODO REVISION
    * What is the thing we want to display in the table?  (course model)
    * What is the listener type we need to implement? (course listener)
    * Use FireTableDataChanged() when need to update the table
    * What is the thing we want to display in one single row? (student result)
    * */

    // The purpose is to convert data into displayable table format
    // Currently data from the Course object

    // Get data from Course object
    private Course _course;

    public CourseAdapter(Course course) {
        this._course = course;
        _course.addCourseListener(this);
    }

    // Called when there is a change in the course object
    // When the course object change, notify the table
    @Override
    public void courseHasChanged(Course course) {
        fireTableDataChanged();
    }

    // Get the total amount of rows in the table model
    @Override
    public int getRowCount() {
        return _course.size(); // dynamic
    }

    // Get the number of columns in the table model
    @Override
    public int getColumnCount() {
        return 7; // fixed. no. of columns don't change - same type of data for each entry in the table
    }

    // Returning the value of the given cell
    // Row index will give the student result
    // Column index will give the particular type of data
    // Refer to assignment handout for type of data we want for each column
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        StudentResult studentRow = _course.getResultAt(rowIndex);

        if (columnIndex == 0) {
            return studentRow._studentID;
        }

        if (columnIndex == 1) {
            return studentRow._studentSurname;
        }

        if (columnIndex == 2) {
            return studentRow._studentForename;
        }

        if (columnIndex == 3) {
            return studentRow.getAssessmentElement(StudentResult.AssessmentElement.Exam);
        }

        if (columnIndex == 4) {
            return studentRow.getAssessmentElement(StudentResult.AssessmentElement.Test);
        }

        if (columnIndex == 5) {
            return studentRow.getAssessmentElement(StudentResult.AssessmentElement.Assignment);
        }

        if (columnIndex == 6) {
            return studentRow.getAssessmentElement(StudentResult.AssessmentElement.Overall);
        }

        return null;
    }

    // Set the names for each column index
    // Access the column using the given index, return the name of the column as String
    @Override
    public String getColumnName(int columnIndex) {

        if (columnIndex == 0) {
            return "Student ID";
        }

        if (columnIndex == 1) {
            return "Surname";
        }

        if (columnIndex == 2) {
            return "Forename";
        }

        if (columnIndex == 3) {
            return "Exam";
        }

        if (columnIndex == 4) {
            return "Test";
        }

        if (columnIndex == 5) {
            return "Assignment";
        }

        if (columnIndex == 6) {
            return "Overall";
        }

        return null;
    }
}